/*
ex0040_lecture_bouton
Lecture de l'état du bouton connecté à la PIN 12
S'il est pressé, allume la LED connectée à la PIN 13 durant un instant
*/

#define pinOut 13 // Définition de constantes.
#define pinIn 12

void setup() {
//============
// Initialise la PIN digital 13 comme OUTPUT
// La PIN 13 a une LED connectée sur la plupart des cartes Arduino
// Initialise la PIN digital pinIn comme INPUT
pinMode(pinOut, OUTPUT);
pinMode(pinIn, INPUT);
digitalWrite(pinOut, LOW);    // set the LED off
} // setup

void loop() {
//===========
if (digitalRead(pinIn) == 1) {
  // On a pressé sur le bouton qui met la PIN pinIn à l'état haut (HIGH)
  digitalWrite(pinOut, HIGH);  // set LED on
  delay(1000);             // wait for some milliseconds
  }

digitalWrite(pinOut, LOW);    // set the LED off
delay(50); // Juste pour voir qu'elle s'éteint, meme si le bouton reste pressé
} // loop
