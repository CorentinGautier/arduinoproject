#include "DHT.h"   // Librairie des capteurs DHT
#define DHTPIN 2    // Changer le pin sur lequel est branché le DHT
#define DHTTYPE DHT22      // DHT 22  (AM2302)

float tableauT[10];
int nbKey = 0;

DHT dht(DHTPIN, DHTTYPE);
void setup() {
  Serial.begin(9600);
  Serial.println("c'est partie !");

  dht.begin();
}

void loop() {
  delay(2000);

  // Lecture du taux d'humidité
  float h = dht.readHumidity();
  // Lecture de la température en Celcius
  float t = dht.readTemperature();
  // Pour lire la température en Fahrenheit
  float f = dht.readTemperature(true);

  // Stop le programme et renvoie un message d'erreur si le capteur ne renvoie aucune mesure
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Echec de lecture !");
    return;
  }

  // Calcul la température ressentie. Il calcul est effectué à partir de la température en Fahrenheit
  // On fait la conversion en Celcius dans la foulée
  float hi = dht.computeHeatIndex(f, h);


  Serial.print("Humidite: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print("Temperature ressentie: ");
  Serial.print(dht.convertFtoC(hi));
  Serial.println(" *C");
  
   //stokage
   tableauT[nbKey] = t;
   nbKey = nbKey + 1;
   
   Serial.print("Température : ");
   for(int i = 0; i <= 9; i = i + 1){
    Serial.print(tableauT[i]);
    Serial.print(", ");
   }
    delay(3000);
}
