

int ledPin= 10;
int  readValue = 0;
int ledVal = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode (ledPin, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  readValue = analogRead(A0);
  ledVal = map(readValue, 0 , 1024, 0 , 255);
  analogWrite(ledPin, ledVal);
}
