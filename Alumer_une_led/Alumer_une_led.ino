/*
ex0010_blink_pin13
Fait clignoter la LED de la PIN 13 régulièrement
*/

void setup() {
//============
// Initialise la PIN digital 13 comme OUTPUT
// La PIN 13 a une LED connectée sur la plupart des cartes Arduino
pinMode(13, OUTPUT);
}

void loop() {
//===========
digitalWrite(13, HIGH);  // set LED on
delay(500);              // wait for some milliseconds
digitalWrite(13, LOW);    // set the LED off
delay(500);              // wait for some milliseconds
}
