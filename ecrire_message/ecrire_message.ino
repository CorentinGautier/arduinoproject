/*
  ex0130_liquid_crystal_hello_world
  Utilise l'affichage LCD (à cristaux liquides),
  pour afficher du texte.
  De plus, il existe un "shield" qui ajoute 5 boutons en plus du bouton reset,
  pour avoir des entrées possibles.
  Ainsi, l'Arduino devient autonome pour beaucoup d'applications,
  sans nécessité d'un ordinateur pour l'utiliser et
  afficher des résultats.

  La lecture des boutons est lente, elle prend plus de 5 milli-secondes,
  mais elle se fait en un seul appelle à la fonction : read_LCD_buttons()
  Pour une version que je préfère, c.f. : ex0131_liquid_crystal_hello_world

  Ici, l'afficheur écrit "Hello World!" et "Bonjour a tous!"
  Puis mesure le temps et indique quelle touche est pressée.

  Affichage d'un texte.
  Lecture de l'état des Pushbutton.

  Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
  library works with all LCD displays that are compatible with the
  Hitachi HD44780 driver. There are many of them out there, and you
  can usually tell them by the 16-pin interface.

  This sketch prints "Hello World!" to the LCD
  and shows the time.

  The circuit:
   LCD RS pin to digital pin 8
   LCD Enable pin to digital pin 9
   LCD D4 pin to digital pin 4
   LCD D5 pin to digital pin 5
   LCD D6 pin to digital pin 6
   LCD D7 pin to digital pin 7
   10K resistor:
   ends to +5V and ground
   wiper to LCD VO pin (pin 3)

  http://www.arduino.cc/en/Tutorial/LiquidCrystal

  c.f. :
  https://arduino-info.wikispaces.com/LCD-Pushbuttons

  PINs qui restent libre et utilisables :
  13, 12, 11, 3, 2, 1 et 0
  La PIN 10 sert à allumer ou éteindre la "backlight", la rétro - lumière
  pinMode(10, OUTPUT);  // Permet d'éteindre ou d'allumer la "backlight"
  digitalWrite(10, HIGH);   // allume la "backlight"
  digitalWrite(10, LOW);   // éteind la "backlight"

  Les PINs  0  et  1  sont utilisés pour la transmission série par USB,
*/

// Inclus la librairie qui gère l'affichage
#include <LiquidCrystal.h>

// Déclaration de Constantes
#define btnNONE   0
#define btnSELECT 1
#define btnLEFT   2
#define btnDOWN   3
#define btnUP     4
#define btnRIGHT  5

int nNumButton = 0;  // N° du bouton pressé
int nNumButtonLast = 0;  // N° du dernier bouton pressé

// Initialisation, pour indiquer quelles sont les PINs utilisées par l'afficheur LCD
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
// c.f. : https://www.arduino.cc/en/Reference/StringObject

char acStr[20];   // Un "array" de caractères, pour conversion de nombre en un String.
String strS = ""; // Une chaine de caractères.

// Indique le bouton pressé
String astrS[6];  // Indices vont de astrS[0] à astrS[5],  l'indice  6  n'est pas valide !

void setup() {
  //============
  // Indique que l'afficheur LCD a deux lignes de 16 caractères
  lcd.begin(8, 1);
  // Affiche un message au départ, pour vérifier le bon fonctionnement.
  lcd.setCursor(0, 0); // Première colonne, première ligne
  lcd.print("hello, world!");
  lcd.setCursor(0, 1); // Première colonne, deuxième ligne
  lcd.print("bonjour a tous!");  // Les caractères accentués ne passent pas.
  delay(2000);
  lcd.setCursor(0, 0);           // Première ligne
  //lcd.print("                "); // efface le texte 16 caractères
  lcd.setCursor(0, 1);           // Deuxième ligne
  //lcd.print("                "); // efface le texte 16 caractères
  //lcd.autoscroll();

  // Texte qui peut etre affiché
  astrS[btnNONE]   = "None  ";
  astrS[btnSELECT] = "Select";
  astrS[btnLEFT]   = "Left  ";
  astrS[btnDOWN]   = "Down  ";
  astrS[btnUP]     = "Up    ";
  astrS[btnRIGHT]  = "Right ";

  pinMode(10, OUTPUT); // Permet d'allumer ou d'éteindre le rétro-éclairage.
  digitalWrite(10, HIGH); // allume le rétro-éclairage
} // setup

int read_LCD_buttons() {
  // ======================
  // Lecture du bouton appuié
  int nAdc_key_in = analogRead(0); // Lecture du bouton pressé
  delay(5); // Attente, pour laisser des rebonds se terminer.
  int nAdc_Delta = (analogRead(0) - nAdc_key_in); // Deuxième lecture du bouton, pour vérification.
  if (5 < abs(nAdc_Delta)) return btnNONE;  // Si la deuxième lecture est trop différente de la première, ignore le bouton.
  // Mes valeurs lues sont :                            0, 130, 306, 479,  720,  1023
  //                                                  Right Up Down Left  Select none
  // On additionne environ 50 à ces valeurs pour tester dans quelle tranche la valeur a été lue
  if (nAdc_key_in > 800) return btnNONE; // Ce test vient en premier, car c'est la réponse la plus habituelle
  if (nAdc_key_in > 550) return btnSELECT; // entre 479 et 800
  if (nAdc_key_in > 380) return btnLEFT;   // entre 306 et 479
  if (nAdc_key_in > 180) return btnDOWN;   // entre 130 et 306
  if (nAdc_key_in >  70) return btnUP;     // entre   0 et 130
  return btnRIGHT;  // adc__key_in est plus petit que 71
}

void loop() {
  //===========
  // Affichage du temps sur la 2ème ligne
  lcd.setCursor(0, 1);

  // Affiche le temps en secondes, depuis le départ.
  //delay(100);
  // Pour sprintf, c.f. https://www.tutorialspoint.com/c_standard_library/c_function_sprintf.htm
  // %d ou %i pour signed decimal integer
  // %u pour unsigned decimal integer
  // %ld ou %li pour long signed decimal integer
  // %lu pour long unsigned decimal integer
  //sprintf(acStr, "%8.3f", millis()/1000.0); // Ne fonctionne pas
  //sprintf(acStr, "%9lu", millis()); // Fonctionne bien
  dtostrf(millis() / 1000.0, 12, 3, acStr); // Marche bien pour convertir un float en un string, avec un format
  // millis()  retourne le nombre de millisecondes écoulé depuis la mise sous tension ou le reset de l'Arduino
  lcd.print(acStr);

  // La lecture des boutons se fait par l'intermédiaire du port A0
  int sensorValue = analogRead(0);
  //sprintf(acStr, "%4d  %4d    ", sensorValue, read_LCD_buttons()); // Fonctionne bien
  sprintf(acStr, "%4d ", sensorValue);
  if (nNumButton != btnNONE) nNumButtonLast = nNumButton; // mémorise le dernier bouton pressé
  nNumButton = read_LCD_buttons();  // Lecture du bouton pressé
  strS = acStr + astrS[nNumButton];
  lcd.setCursor(0, 0);
  lcd.print(strS);

  if (nNumButton == btnSELECT) {
    if (nNumButtonLast == btnDOWN) digitalWrite(10, LOW); // éteind le rétro-éclairage
    if (nNumButtonLast == btnUP) digitalWrite(10, HIGH); // allume le rétro-éclairage
  }
} // loop
