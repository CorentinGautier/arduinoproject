// lecture hygromètre
#include "DHT.h"   // Librairie des capteurs DHT
#define DHTPIN 2    // Changer le pin sur lequel est branché le DHT
#define DHTTYPE DHT22      // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);

//lecture telementre
#include <Wire.h>
const char DOUT_TRIGGER = 7;
const char DIN_ECHO = 6;
float distance;
float tableauT[10];
float tableauD[10];
int nbKey = 0;
//bouton
const int btnPin  = A0;
int btnVal  = 0;

//stockage
#include <EEPROM.h>
void setup() {

//setup affichage
Serial.begin(9600);
Serial.println("Démarrage");

//setup button
//pinMode(btnPin, INPUT);
pinMode(btnPin,INPUT_PULLUP); 
Serial.println("bouton OK");

//setup telemetre
pinMode(DIN_ECHO, INPUT);
pinMode(DOUT_TRIGGER, OUTPUT);
Serial.println("telemetre OK");

//setup hygrometre
dht.begin();
Serial.println("hygrometre OK");

//setup stockage
EEPROM.length();

  }

void loop() {


  
btnVal=analogRead(btnPin);
if(btnVal<200){
Serial.print("button appuyé");
// lecture hygromètee
      Serial.println("lecture hygromètee");
      // Lecture du taux d'humidité
      float h = dht.readHumidity();
      // Lecture de la température en Celcius
      float t = dht.readTemperature();
       // Pour lire la température en Fahrenheit
      float f = dht.readTemperature(true);

      
      // Stop le programme et renvoie un message d'erreur si le capteur ne renvoie aucune mesure
      if (isnan(h) || isnan(t) || isnan(f)) {
        Serial.println("Echec de lecture !");
        return;
      }

      // Calcul la température ressentie. Il calcul est effectué à partir de la température en Fahrenheit
      // On fait la conversion en Celcius dans la foulée
      float hi = dht.computeHeatIndex(f, h);

       Serial.print("Humidite: ");
       Serial.print(h);
       Serial.println(" %\t");
       Serial.print("Temperature: ");
       Serial.print(t);
       Serial.println(" *C ");
       Serial.print("Temperature ressentie: ");
       Serial.print(dht.convertFtoC(hi));
       Serial.println(" *C");

  Serial.println("fin lecture hygromètee"); 
  //lecture telemetre
  Serial.println("lecture telemetre");
      digitalWrite(DOUT_TRIGGER, LOW);
      delayMicroseconds(2);
      digitalWrite(DOUT_TRIGGER, HIGH);
      delayMicroseconds(10);
      digitalWrite(DOUT_TRIGGER, LOW);
      distance = pulseIn(DIN_ECHO, HIGH) / 60.0;
      if (distance > 400) {
        Serial.print("echec de la mesure");
        delay(800);
      }
      else {
        Serial.println("distance");
        Serial.print(distance);
        Serial.println("cm");
        delay(2000);
     }
   Serial.println("fin lecture telemetre");

   //stokage
   tableauT[nbKey] = t;
   tableauD[nbKey] = distance;
   nbKey = nbKey + 1;
   
   Serial.print("Température : ");
   for(int i = 0; i <= 9; i = i + 1){
    Serial.print(tableauT[i]);
    Serial.print(", ");
   }
   Serial.println("");
   Serial.print("Distance : ");
   for(int i = 0; i <= 9; i = i + 1){
    Serial.print(tableauD[i]);
    Serial.print(", ");
   }

   delay(3000);
  }
  Serial.println("");
 Serial.println("--------------------------");
 delay(3000);
}
