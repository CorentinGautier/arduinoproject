#include <Arduino.h>
#include "DHT.h"   // Librairie des capteurs DHT
#define DHTPIN 13    // Changer le pin sur lequel est branché le DHT
#define DHTTYPE DHT22      // DHT 22  (AM2302)
#include <ESP8266WiFi.h> // Load Wi-Fi library

// Replace with your network credentials
const char* SSID = "TP-LINK_F016"; 
const char* PASSWORD = "94436778";

// Set your Static IP address
IPAddress local_IP(192, 168, 0, 78);
// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);

// Set web server port number to 80
WiFiServer server(80);

// Assign output variables to GPIO pins
const int output = 4;

int potentValue = 0;

void setup() {
  pinMode(4, OUTPUT);
  Serial.begin(9600);
    while (!Serial);
  // Initialize the output and set it to LOW
  pinMode(output, OUTPUT);
  digitalWrite(output, LOW);
    if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }

  // Connect to Wi-Fi network with SSID and PASSWORD
  Serial.print("Connecting to ");
  Serial.println(SSID);
  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address 
  Serial.println("");
  Serial.println("WiFi connected at IP address:");
  Serial.println(WiFi.localIP());

  // Start Web Server
  server.begin();

  float tableauT[10];
  int nbKey = 0;
}

void loop(){
  // Create a client and listen for incoming clients
  WiFiClient client = server.available();   
  
  // Do nothing if server is not available
  if (!client) {
     return;
  }
  
  // Wait a client 
  while(!client.available()){;}
  
  // A new client is connected, get the request
  Serial.println("j'ai reçu une requête GET");
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();
  
  DHT dht(DHTPIN, DHTTYPE);
  dht.begin();
  
  Serial.println("lecture des sensors");
    // Lecture du taux d'humidité
  float h = dht.readHumidity();
  // Lecture de la température en Celcius
  float t = dht.readTemperature();

  // Stop le programme et renvoie un message d'erreur si le capteur ne renvoie aucune mesure
  if (isnan(h) || isnan(t)) {
    Serial.println("Echec de lecture !");
    return;
  }
  // Display GPIO status
  client.println("HTTP/1.1 200 OK\r\n"
  "Access-Control-Allow-Origin: *\r\n"
  "Access-Control-Allow-Methods: POST, GET\r\n"
  "Access-Control-Allow-Headers: X-PINGOTHER, Content-Type\r\n"
  "Access-Control-Max-Age: 86400\r\n"
  "Content-Type: text/html\r\n");

  client.println("\r\n<!DOCTYPE HTML>");
  client.println("<html>");
  client.println(t);
  client.println(",");
  client.println(h);
  client.println("</html>");
  return;
}
