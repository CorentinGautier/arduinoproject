#include <Wire.h>
const char DOUT_TRIGGER = 7;
const char DIN_ECHO = 6;
float distance;

void setup() {
  Serial.begin(9600);
  Serial.print("Bonjour !");
  delay(2000);
  Serial.println("debut mesure !");
  pinMode(DIN_ECHO, INPUT);
  pinMode(DOUT_TRIGGER, OUTPUT);
  delay(1000);
}

void loop() {
  digitalWrite(DOUT_TRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(DOUT_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(DOUT_TRIGGER, LOW);
  distance = pulseIn(DIN_ECHO, HIGH) / 60.0;
  if (distance > 400) {
    Serial.print("echec de la ");
    Serial.print("mesure");
    delay(800);
  }
  else {
    Serial.println("distance");
    Serial.print(distance);
    Serial.print("cm");
    delay(2000);
  }
}
