// lecture hygromètre
#include <avr/sleep.h>// librairie à installer
#define interruptPin 3 //Pin 2 ou 3 pour un bouton ou le capteur de mouvement

//stockage
#include <EEPROM.h>

void setup() {
  Serial.begin(9600);// Communication série
  pinMode(LED_BUILTIN,OUTPUT);// pin 13 pour indiquer l'état
  pinMode(interruptPin,INPUT_PULLUP);// pin3  avec un bouton
  digitalWrite(LED_BUILTIN,HIGH);//témoin allumé
}

void loop() {
  delay(3000);// 3 secondes d'activité
  miseEnSommeil();
}

void miseEnSommeil(){
    Serial.println("hello");
    sleep_enable();//on active le sleep mode
    attachInterrupt(1, wakeUp, LOW);// pin 3
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);// choix du type de sommeil profond
    digitalWrite(LED_BUILTIN,LOW);//led éteinte
    delay(500); // pause d'une demie seconde
    sleep_cpu();//passage en sommeil 
    Serial.println("de retour");//ligne executée après l'interrupt 
    digitalWrite(LED_BUILTIN,HIGH);// rallumage du témoin
}

void wakeUp(){
  Serial.println("interruption");// message à la console
  sleep_disable();//désactivation du sommeil
  detachInterrupt(1); //on enlève l'interruption du pin 2;
}
